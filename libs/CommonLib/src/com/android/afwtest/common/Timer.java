/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.common;

import android.util.Log;

import java.util.concurrent.TimeUnit;

/**
 * A count-down timer.
 */
public final class Timer {

    private static final String TAG = "afwtest.Timer";

    // A year in milliseconds
    private static final long ONE_YEAR_IN_MS = TimeUnit.DAYS.toMillis(365);

    // Timeout of this timer in milliseconds
    private final long mTimeoutMs;

    // Start time in milliseconds since Jan 1, 1970
    private long mStartTime;

    // Elapsed time so far, in milliseconds
    private long mElapsedTime;

    /**
     * Constructor.
     *
     * @param timeout timeout in milliseconds
     */
    public Timer(long timeout) {
        mTimeoutMs = timeout;
    }

    /**
     * Starts this timer.
     */
    public void start() {
        mStartTime = System.currentTimeMillis();
        mElapsedTime = 0;
    }

    /**
     * Checks if time is up by checking if elapsed time is greater than timeout.
     *
     * @return {@code true} if time is up, {@code false} otherwise
     */
    public boolean isTimeUp() {
        elapse();
        return mElapsedTime > mTimeoutMs;
    }

    /**
     * Gets elapsed time since start().
     *
     * @return long, elapsed time in milliseconds.
     */
    public long elapsedTime(){
        elapse();
        return mElapsedTime;
    }

    /**
     * Re-calculate elapsed time.
     */
    private void elapse() {
        long currentTime = System.currentTimeMillis();
        long elapsedTime = currentTime - mStartTime;
        // The system time might change suddenly from 1970 to 21 century, e.g. after factory reset.
        // Reset mStartTime without updating mElapsedTime.
        if (elapsedTime > ONE_YEAR_IN_MS) {
            // Reset time
            mStartTime = currentTime - mElapsedTime;
            Log.w(TAG, String.format("System time changed: delta=%d", elapsedTime));
        } else {
            mElapsedTime = elapsedTime;
        }
    }
}

/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

/**
 * Network utilities.
 */
public class NetworkUtils {
    private static final String TAG = "afwtest.NetworkUtils";

    /**
     * Enables Wifi.
     *
     * @param context {@link Context} object
     * @return {@code true} if Wifi is enabled successfully; {@code false} otherwise
     */
    public static boolean enableWifi(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        return wifiManager != null
                && (wifiManager.isWifiEnabled() || wifiManager.setWifiEnabled(true));
    }

    /**
     * Returns whether the device is currently connected to a wifi.
     *
     * @param context {@link Context} object
     * @return {@code true} if connected to Wifi; {@code false} otherwise
     */
    public static boolean isConnectedToWifi(Context context) {
        NetworkInfo info = getActiveNetworkInfo(context);
        return info != null
                && info.isConnected()
                && info.getType() == ConnectivityManager.TYPE_WIFI;
    }

    /**
     * Checks if connected with expected wifi.
     *
     * @param context {@link Context} object
     * @param ssid Wifi SSID
     * @return {@code true} if the expected wifi
     */
    public static boolean isConnectedToSpecifiedWifi(Context context, String ssid) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        return ssid != null
                && wifiManager != null
                && isConnectedToWifi(context)
                && wifiManager.getConnectionInfo() != null
                && ssid.equals(wifiManager.getConnectionInfo().getSSID());
    }

    /**
     * Gets the active network.
     *
     * @param context {@link Context} object
     * @return active {@link NetworkInfo}
     */
    private static NetworkInfo getActiveNetworkInfo(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            return cm.getActiveNetworkInfo();
        }
        return null;
    }

    /**
     * Disconnects a device from the connected Wi-Fi network by removing the network configuration.
     *
     * @param context {@link Context} object
     */
    public static boolean disconnectFromWifi(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager.isWifiEnabled() && isConnectedToWifi(context)) {
            String wifiSsid = wifiManager.getConnectionInfo().getSSID();
            if (!wifiManager.removeNetwork(wifiManager.getConnectionInfo().getNetworkId())) {
                Log.e(TAG, String.format("Failed to remove Wifi %s", wifiSsid));
                return false;
            }
        }

        return true;
    }
}

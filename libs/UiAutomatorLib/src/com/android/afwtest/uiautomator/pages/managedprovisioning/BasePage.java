/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.pages.managedprovisioning;

import static com.android.afwtest.uiautomator.Constants.MANAGED_PROVISIONING_ERROR_MSG_SELECTOR;
import static com.android.afwtest.uiautomator.Constants.MANAGED_PROVISIONING_OK_BUTTON_SELECTOR;
import static com.android.afwtest.uiautomator.Constants.MANAGED_PROVISIONING_PKG_NAME;
import static com.android.afwtest.uiautomator.Constants.MANAGED_PROVISIONING_PKG_SELECTOR;

import android.os.SystemClock;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;

import com.android.afwtest.common.Timer;
import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.pages.UiPage;
import com.android.afwtest.uiautomator.utils.WidgetUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Base class for all Managed Provisioning pages to keep common functionality in one place.
 */
public abstract class BasePage extends UiPage {

    /**
     * Thread sleep time, in milliseconds.
     */
    protected static final long THREAD_SLEEP_TIME_MS = TimeUnit.SECONDS.toMillis(1);

    private static final BySelector LEARN_MORE_TEXT_1 =
            By.res(MANAGED_PROVISIONING_PKG_NAME, "learn_more_text1");

    /**
     * Default provisioning timeout in milliseconds.
     */
    private static final long DEFAULT_PROVISIONING_TIMEOUT_MS = TimeUnit.MINUTES.toMillis(1);

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     */
    public BasePage(UiDevice uiDevice, TestConfig config) {
        super(uiDevice, config);
    }

    /**
     * Gets provisioning error message.
     *
     * @return Error message if provisioning is showing error dialog, {@code null} otherwise
     */
    protected String getProvisioningError() {
        // Get any managed provisioning error dialog
        if (getUiDevice().hasObject(MANAGED_PROVISIONING_ERROR_MSG_SELECTOR)) {
            return getUiDevice().findObject(MANAGED_PROVISIONING_ERROR_MSG_SELECTOR).getText();
        }

        return null;
    }

    /**
     * Gets the provisioning timeout in millisecond.
     *
     * @return provisioning timeout in millisecond
     */
    protected long getProvisioningTimeoutInMs() throws IOException {
        return DEFAULT_PROVISIONING_TIMEOUT_MS * getTestConfig().getTimeoutSize();
    }

    /**
     * Waits for the provisioning to finish.
     *
     * @param timeout Waiting timeout in milliseconds
     * @return {@code true} if the provisioning is finished, {@code false} otherwise
     */
    protected boolean waitForProvisioningToFinish() throws Exception {
        Timer timer = new Timer(getProvisioningTimeoutInMs());
        timer.start();
        while (!timer.isTimeUp()) {
            if (WidgetUtils.safeWait(getUiDevice(), MANAGED_PROVISIONING_PKG_SELECTOR) == null) {
                assertOnFatalAppCrash();
                return true;
            }

            // Dismiss the dialog with: "Your administrator has the ability to monitor...".
            if (getUiDevice().hasObject(LEARN_MORE_TEXT_1)) {
                getUiDevice().findObject(MANAGED_PROVISIONING_OK_BUTTON_SELECTOR).click();
                SystemClock.sleep(THREAD_SLEEP_TIME_MS);
                continue;
            }

            String errorMsg = getProvisioningError();
            if (errorMsg != null) {
                throw new RuntimeException("Provisioning error: " + errorMsg);
            }

            assertOnFatalAppCrash();

            SystemClock.sleep(THREAD_SLEEP_TIME_MS);
        }

        return false;
    }
}

/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.pages.managedprovisioning;

import static com.android.afwtest.uiautomator.Constants.MANAGED_PROVISIONING_PKG_NAME;
import static com.android.afwtest.uiautomator.Constants.MANAGED_PROVISIONING_SCROLL_VIEW_SELECTOR;
import static com.android.afwtest.uiautomator.Constants.MANAGED_PROVISIONING_SETUP_BUTTON_SELECTOR;
import static com.android.afwtest.uiautomator.Constants.STAT_TESTDPC_WORK_PROFILE_CREATION_TIME;
import static java.util.regex.Pattern.CASE_INSENSITIVE;

import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;
import android.widget.CheckBox;

import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.utils.WidgetUtils;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * Managed Provisioning setup your device page.
 */
public class SetupYourDevicePage extends BasePage {

    /**
     * Default UI waiting time, in milliseconds.
     */
    private static final long DEFAULT_TIMEOUT_MS = TimeUnit.SECONDS.toMillis(20);

    /**
     * {@link BySelector} unique to this page.
     */
    private static final BySelector SET_UP_YOUR_DEVICE_PAGE_SELECTOR =
            By.pkg(MANAGED_PROVISIONING_PKG_NAME)
                    .text(Pattern.compile("Set up .* device", CASE_INSENSITIVE));

    private static final BySelector LEARN_MORE_TEXT_1 =
            By.res(MANAGED_PROVISIONING_PKG_NAME, "learn_more_text1");

    /**
     * {@link BySelector} for {@link CheckBox} with resource-id user_consent_checkbox on
     * base ManagedProvisioning dialog.
     */
    private static final BySelector MANAGED_PROVISIONING_CONSENT_CHECKBOX_SELECTOR =
            By.res(MANAGED_PROVISIONING_PKG_NAME, "user_consent_checkbox")
                    .clazz(CheckBox.class.getName())
                    .checkable(true);

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     */
    public SetupYourDevicePage(UiDevice uiDevice, TestConfig config) {
        super(uiDevice, config);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BySelector uniqueElement() {
        return SET_UP_YOUR_DEVICE_PAGE_SELECTOR;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void navigate() throws Exception {
        WidgetUtils.waitAndClick(getUiDevice(), MANAGED_PROVISIONING_SETUP_BUTTON_SELECTOR,
                DEFAULT_TIMEOUT_MS);
        if (null != WidgetUtils.safeWait(getUiDevice(), LEARN_MORE_TEXT_1)) {
            WidgetUtils.scrollToItem(getUiDevice(),
                    MANAGED_PROVISIONING_SCROLL_VIEW_SELECTOR,
                    MANAGED_PROVISIONING_CONSENT_CHECKBOX_SELECTOR);
            WidgetUtils.waitAndClick(getUiDevice(),
                    MANAGED_PROVISIONING_CONSENT_CHECKBOX_SELECTOR,
                    DEFAULT_TIMEOUT_MS);
        }

        onProvisioningStarted();

        // Wait for the provisioning to finish.
        if (!waitForProvisioningToFinish()) {
            throw new RuntimeException("DO Provisioning timeout");
        }
    }

    /**
     * Handles provisioning started event.
     */
    protected void onProvisioningStarted() throws Exception {
        // Default to be TestDpc provisioning
        getProvisioningStatsLogger().startTime(STAT_TESTDPC_WORK_PROFILE_CREATION_TIME);
    }
}

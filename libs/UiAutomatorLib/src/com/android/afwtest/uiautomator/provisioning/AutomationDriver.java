/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.provisioning;

import static com.android.afwtest.uiautomator.Constants.MANAGED_PROVISIONING_PKG_NAME;
import static com.android.afwtest.uiautomator.Constants.TESTDPC_PKG_NAME;
import static com.android.afwtest.uiautomator.pages.gms.AddAccountPage.GMS_ADD_OR_VERIFY_ACCOUNT_PAGE_SELECTOR;

import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.util.Log;
import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.pages.LandingPage;
import com.android.afwtest.uiautomator.pages.PageSkipper;
import com.android.afwtest.uiautomator.pages.UiPage;
import com.android.afwtest.uiautomator.pages.gms.AddAccountPage;
import com.android.afwtest.uiautomator.pages.gms.EnterPasswordPage;
import com.android.afwtest.uiautomator.pages.gms.GoogleAppsDevicePolicyPage;
import com.android.afwtest.uiautomator.pages.gms.GoogleServicesPage;
import com.android.afwtest.uiautomator.pages.managedprovisioning.ErrorPage;
import com.android.afwtest.uiautomator.pages.managedprovisioning.SetupYourDevicePage;
import com.android.afwtest.uiautomator.pages.managedprovisioning.TermsPage;
import com.android.afwtest.uiautomator.pages.packageinstaller.DeviceAccessPage;
import com.android.afwtest.uiautomator.pages.testdpc.FinishSetupPage;
import com.android.afwtest.uiautomator.pages.testdpc.SetupManagementDoPage;
import com.android.afwtest.uiautomator.pages.testdpc.SetupManagementPoPage;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * A help class that automates provisioning flows.
 */
public class AutomationDriver {

    private static final String TAG = "afwtest.AutomationDriver";

    /**
     * {@link UiDevice} object.
     */
    private final UiDevice mUiDevice;

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     */
    public AutomationDriver(UiDevice uiDevice) {
        mUiDevice = uiDevice;
    }

    /**
     * Gets {@link UiDevice} instance.
     *
     * @return {@link UiDevice} instances
     */
    protected UiDevice getUiDevice() {
        return mUiDevice;
    }

    /**
     * Runs the NFC provisioning flow.
     *
     * @param testConfig test configurations required for provisioning automation
     * @return {@code true} if provisioning completes, {@code false} otherwise
     */
    public boolean runNfcProvisioning(TestConfig testConfig) throws Exception {
        return navigate(getNfcProvisioningPages(testConfig));
    }

    /**
     * Runs the QR Code provisioning flow.
     *
     * @param testConfig test configurations required for provisioning automation
     * @return {@code true} if provisioning completes, {@code false} otherwise
     */
    public boolean runQRCodeProvisioning(TestConfig testConfig) throws Exception {
        return navigate(getQRCodeProvisioningPages(testConfig));
    }

    /**
     * Runs the setup wizard device owner provisioning flow.
     *
     * @param testConfig test configurations required for provisioning automation
     * @return {@code true} if provisioning completes, {@code false} otherwise
     */
    public boolean runSuwDoProvisioning(TestConfig testConfig) throws Exception {
        return navigate(getSuwDoProvisioningPages(testConfig));
    }

    /**
     * Runs the setup wizard profile owner provisioning flow.
     *
     * @param testConfig test configurations required for provisioning automation
     * @return {@code true} if provisioning completes, {@code false} otherwise
     */
    public boolean runSuwPoProvisioning(TestConfig testConfig) throws Exception {
        return navigate(getSuwPoProvisioningPages(testConfig));
    }

    /**
     * Runs the non setup wizard profile owner provisioning flow.
     *
     * @param testConfig test configurations required for provisioning automation
     * @return {@code true} if provisioning completes, {@code false} otherwise
     */
    public boolean runNonSuwPoProvisioning(TestConfig testConfig) throws Exception {
        return navigate(getNonSuwPoProvisioningPages(testConfig));
    }

    /**
     * Runs the non setup wizard profile owner provisioning flow when it is not allowed.
     *
     * @param testConfig test configurations required for provisioning automation
     * @return {@code true} if provisioning completes, {@code false} otherwise
     */
    public boolean runNonSuwPoProvisioningDisallowed(TestConfig testConfig) throws Exception {
        return navigate(getNonSuwPoProvisioningDisallowedPages(testConfig));
    }

    /**
     * Navigates a list of {@link UiPage} one by one. Optional pages will be skipped if not found.
     *
     * @param pages list of {@link UiPage} to navigate
     *
     * @return {@code true} if all pages are navigated successfully, {@code false} otherwise
     */
    protected boolean navigate(List<UiPage> pages) throws Exception {
        for (UiPage page : pages) {
            if (page.waitForLoading()) {
                Log.i(TAG, String.format("Navigating: %s", page.getClass().getName()));
                page.navigate();
                Log.i(TAG, String.format("Navigating: %s done", page.getClass().getName()));
            } else if (page.isOptional()) {
                Log.i(TAG, String.format("Failed to load page: %s; but it's optional, skipping",
                    page.getClass().getName()));
            } else {
                throw new Exception(
                    String.format("Failed to load page: %s", page.getClass().getName()));
            }
        }

        return true;
    }

    /**
     * Gets the list of {@link UiPage} for NFC provisioning flow.
     *
     * @param testConfig test configurations required for provisioning automation
     * @return list of {@link UiPage} representing the setup wizard DO provisioning flow
     */
    private List<UiPage> getNfcProvisioningPages(TestConfig testConfig) {
        List<UiPage> pages = new LinkedList<>();
        pages.add(new SetupYourDevicePage(mUiDevice, testConfig));

        // Land on the mdm page.
        UiPage mdmPage =
                new LandingPage(mUiDevice, testConfig, By.pkg(testConfig.getDeviceAdminPkgName()));
        PageSkipper pageSkipper = new PageSkipper(mUiDevice, mdmPage.uniqueElement(), testConfig);

        pages.add(pageSkipper);
        pages.add(mdmPage);

        // Setup page skipper.
        Set<String> packageNameBlacklist = new HashSet<String>();
        packageNameBlacklist.add(MANAGED_PROVISIONING_PKG_NAME);
        packageNameBlacklist.add(testConfig.getDeviceAdminPkgName());
        pageSkipper.setPackageNameBlacklist(packageNameBlacklist);

        return pages;
    }

    /**
     * Gets the list of {@link UiPage} for QR code provisioning flow.
     *
     * @param testConfig test configurations required for provisioning automation
     * @return list of {@link UiPage} representing the setup wizard DO provisioning flow
     */
    private List<UiPage> getQRCodeProvisioningPages(TestConfig testConfig) {
        List<UiPage> pages = new LinkedList<>();
        pages.add(new SetupYourDevicePage(mUiDevice, testConfig));

        // Land on the mdm page.
        UiPage mdmPage =
                new LandingPage(mUiDevice, testConfig, By.pkg(testConfig.getDeviceAdminPkgName()));
        PageSkipper pageSkipper = new PageSkipper(mUiDevice, mdmPage.uniqueElement(), testConfig);

        pages.add(pageSkipper);
        pages.add(mdmPage);

        // Setup page skipper.
        Set<String> packageNameBlacklist = new HashSet<String>();
        packageNameBlacklist.add(MANAGED_PROVISIONING_PKG_NAME);
        packageNameBlacklist.add(testConfig.getDeviceAdminPkgName());
        pageSkipper.setPackageNameBlacklist(packageNameBlacklist);

        return pages;
    }

    /**
     * Gets the list of {@link UiPage} for setup wizard DO provisioning flow.
     *
     * @param testConfig test configurations required for provisioning automation
     * @return list of {@link UiPage} representing the setup wizard DO provisioning flow
     */
    private List<UiPage> getSuwDoProvisioningPages(TestConfig testConfig) {
        List<UiPage> pages = new LinkedList<>();
        pages.add(new AddAccountPage(mUiDevice, testConfig));
        pages.add(new EnterPasswordPage(mUiDevice, testConfig));
        pages.add(new GoogleServicesPage(mUiDevice, testConfig));
        pages.add(new GoogleAppsDevicePolicyPage(mUiDevice, testConfig));
        pages.add(new DeviceAccessPage(mUiDevice, testConfig));
        pages.add(new SetupManagementDoPage(mUiDevice, testConfig));
        pages.add(new SetupYourDevicePage(mUiDevice, testConfig));

        UiPage landingPage = new FinishSetupPage(mUiDevice, testConfig);
        PageSkipper pageSkipper =
                new PageSkipper(mUiDevice, landingPage.uniqueElement(), testConfig);
        pages.add(pageSkipper);
        pages.add(landingPage);

        // Setup page skipper.
        Set<String> packageNameBlacklist = new HashSet<String>();
        packageNameBlacklist.add(MANAGED_PROVISIONING_PKG_NAME);
        packageNameBlacklist.add(TESTDPC_PKG_NAME);
        pageSkipper.setPackageNameBlacklist(packageNameBlacklist);

        return pages;
    }

    /**
     * Gets the list of {@link UiPage} for setup wizard PO provisioning flow.
     *
     * @param testConfig test configurations required for provisioning automation
     * @return list of {@link UiPage} representing the setup wizard PO provisioning flow
     */
    private List<UiPage> getSuwPoProvisioningPages(TestConfig testConfig) {
        List<UiPage> pages = new LinkedList<>();
        pages.add(getToAddAccountSkipper(testConfig));
        pages.add(new AddAccountPage(mUiDevice, testConfig));
        pages.add(new EnterPasswordPage(mUiDevice, testConfig));
        pages.add(new GoogleServicesPage(mUiDevice, testConfig));
        pages.add(new GoogleAppsDevicePolicyPage(mUiDevice, testConfig));
        pages.add(new DeviceAccessPage(mUiDevice, testConfig));
        pages.add(new SetupManagementPoPage(mUiDevice, testConfig));
        pages.add(new TermsPage(mUiDevice, testConfig));

        UiPage landingPage = new FinishSetupPage(mUiDevice, testConfig);
        PageSkipper pageSkipper =
                new PageSkipper(mUiDevice, landingPage.uniqueElement(), testConfig);
        pages.add(pageSkipper);
        pages.add(landingPage);

        // Setup page skipper.
        Set<String> packageNameBlacklist = new HashSet<String>();
        packageNameBlacklist.add(MANAGED_PROVISIONING_PKG_NAME);
        packageNameBlacklist.add(TESTDPC_PKG_NAME);
        pageSkipper.setPackageNameBlacklist(packageNameBlacklist);

        return pages;
    }

    /**
     * Gets {@link PageSkipper} to go through Setup Wizard to reach {@link AddAccountPage}.
     *
     * @param testConfig test configurations required for provisioning automation.
     * @return {@link PageSkipper} to go through Setup Wizard to reach {@link AddAccountPage}.
     */
    private UiPage getToAddAccountSkipper(TestConfig testConfig) {
        return new PageSkipper(mUiDevice, GMS_ADD_OR_VERIFY_ACCOUNT_PAGE_SELECTOR, testConfig);
    }

    /**
     * Gets the list of {@link UiPage} for non setup wizard PO provisioning flow.
     *
     * @param testConfig test configurations required for provisioning automation
     * @return list of {@link UiPage} representing the non setup wizard PO provisioning flow
     */
    private List<UiPage> getNonSuwPoProvisioningPages(TestConfig testConfig) {
        List<UiPage> pages = new LinkedList<>();
        pages.add(new AddAccountPage(mUiDevice, testConfig));
        pages.add(new EnterPasswordPage(mUiDevice, testConfig));
        pages.add(new GoogleServicesPage(mUiDevice, testConfig));
        pages.add(new GoogleAppsDevicePolicyPage(mUiDevice, testConfig));
        pages.add(new DeviceAccessPage(mUiDevice, testConfig));
        pages.add(new SetupManagementPoPage(mUiDevice, testConfig));
        pages.add(new TermsPage(mUiDevice, testConfig));
        pages.add(new FinishSetupPage(mUiDevice, testConfig));
        return pages;
    }

    /**
     * Gets the list of {@link UiPage} for testing disallowed provisioning test.
     *
     * @param testConfig test configurations required for provisioning automation
     * @return list of {@link UiPage} for testing disallowed provisioning test.
     */
    private List<UiPage> getNonSuwPoProvisioningDisallowedPages(TestConfig testConfig) {
        List<UiPage> pages = new LinkedList<>();
        pages.add(new ErrorPage(mUiDevice, testConfig));
        return pages;
    }
}

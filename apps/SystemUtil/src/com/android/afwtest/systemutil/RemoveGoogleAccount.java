/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.systemutil;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.app.Instrumentation;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.Semaphore;

/**
 * An instrumentation utility to remove Google account from device.
 */
public class RemoveGoogleAccount extends Instrumentation {

    private static final String TAG = "afwtest.RemoveGoogleAccount";

    // Account to remove; remove all Google accounts if null.
    private String mAccount;

    @Override
    public void onCreate(Bundle arguments) {
        super.onCreate(arguments);
        mAccount = arguments.getString("account", null);
        start();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onStart() {
        super.onStart();

        Log.d(TAG, "Attempting to remove Google accounts");
        Bundle results = new Bundle();

        AccountManager accountManager = AccountManager.get(getContext());
        Account[] accounts = accountManager.getAccountsByType("com.google");
        for (Account account : accounts) {
            if (mAccount == null || account.name.equals(mAccount)) {
                Log.i(TAG, String.format("Removing account %s", account.name));
                RemoveCallback callback = new RemoveCallback();
                accountManager.removeAccount(account, callback, null /* handler */);
                if (!callback.waitForRemoveCompletion()) {
                    String error = String.format("Failed to remove account %s: Reason: %s",
                            account.name, callback.getErrorMessage());
                    results.putString("error", error);
                    finish(Activity.RESULT_CANCELED, results);
                    return;
                }
            }
        }
        results.putString("result", "SUCCESS");
        finish(Activity.RESULT_OK, results);
    }

    /**
     * Callback which will block until account removal result is available or exception throws.
     */
    private static class RemoveCallback implements AccountManagerCallback<Boolean> {

        private static final String TAG = "afwtest.RemoveCallback";

        /**
         * Stores the result of account removal.
         */
        private boolean mResult = false;

        /**
         * Error message if any.
         */
        private String mErrorMessage = null;

        /**
         * {@link Semaphore}, indicating result is available if it's value > 0.
         */
        private Semaphore mLock = new Semaphore(0);

        /**
         * Block and wait for the remove callback to complete.
         *
         * @return the {@link Bundle} result from the remove op.
         */

        public Boolean waitForRemoveCompletion() {
            try {
                // Blocks until semaphore count > 0
                mLock.acquire();
            } catch (InterruptedException e) {
                handleException(e);
            }
            return mResult;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void run(AccountManagerFuture<Boolean> future) {
            try {
                mResult = future.getResult();
            } catch (OperationCanceledException | IOException | AuthenticatorException e) {
                handleException(e);
            } finally {
                // Increment semaphore count by 1.
                mLock.release();
            }
        }

        /**
         * Gets the error message.
         *
         * @return error message
         */
        public String getErrorMessage() {
            return mErrorMessage;
        }

        /**
         * Creates a result bundle for given exception
         */
        private void handleException(Exception e) {
            Log.e(TAG, "Failed to remove account", e);
            mResult = false;
            mErrorMessage = e.toString();
        }
    }
}

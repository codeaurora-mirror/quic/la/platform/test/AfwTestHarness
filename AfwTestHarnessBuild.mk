#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

-include test/AfwTestHarness/AfwTestCaseList.mk

afw_th_src_dir := test/AfwTestHarness
afw_th_dir := $(HOST_OUT)/afw-th
afw_th_tools_src_dir := $(afw_th_src_dir)/tools
afw_th_tests_src_dir := $(afw_th_src_dir)/tests

TH_COMPATIBILITY_HOST_UTIL_JAR := $(HOST_OUT_JAVA_LIBRARIES)/compatibility-host-util.jar
TF_JAR := $(HOST_OUT_JAVA_LIBRARIES)/tradefed.jar
AFW_TH_TF_JAR := $(HOST_OUT_JAVA_LIBRARIES)/afw-test-tradefed.jar
AFW_TH_TF_EXEC_PATH ?= $(HOST_OUT_EXECUTABLES)/afw-test-tradefed
AFW_TH_TF_README_PATH := $(afw_th_tools_src_dir)/tradefed-host/README

AFW_TH_PREBUILT_UTILS_PATH := $(foreach prebuilt, $(afw_th_prebuilt_utils), $(afw_th_tools_src_dir)/prebuilt/$(prebuilt))

$(afw_th_dir)/all_afw_th_files_stamp: $(AFW_TH_TEST_CASES) $(AFW_TH_SUPPORT_PACKAGE_APKS) $(TF_JAR) $(AFW_TH_TF_JAR) $(AFW_TH_TF_EXEC_PATH) $(AFW_TH_TF_README_PATH) $(TH_COMPATIBILITY_HOST_UTIL_JAR) $(ACP)

# Make necessary directory for afw-test
	$(hide) mkdir -p $(TMP_DIR)
	$(hide) mkdir -p $(PRIVATE_DIR)/tools
	$(hide) mkdir -p $(PRIVATE_DIR)/testcases
# Copy executable and JARs to afw-test directory
	$(hide) $(ACP) -fp $(TF_JAR) $(AFW_TH_TF_JAR) $(AFW_TH_TF_EXEC_PATH) $(AFW_TH_TF_README_PATH) $(TH_COMPATIBILITY_HOST_UTIL_JAR) $(PRIVATE_DIR)/tools
	$(hide) $(ACP) -fp $(AFW_TH_PREBUILT_UTILS_PATH) $(PRIVATE_DIR)/testcases
	$(hide) touch $@


afw_th_name := android-afw-test-harness

# Package afw-test-harness and clean up.
#
INTERNAL_AFW_TH_TARGET := $(afw_th_dir)/$(afw_th_name).zip
$(INTERNAL_AFW_TH_TARGET): PRIVATE_NAME := android-cts
$(INTERNAL_AFW_TH_TARGET): PRIVATE_AFW_TH_DIR := $(afw_th_dir)
$(INTERNAL_AFW_TH_TARGET): PRIVATE_DIR := $(afw_th_dir)/android-cts
$(INTERNAL_AFW_TH_TARGET): TMP_DIR := $(afw_th_dir)/temp
$(INTERNAL_AFW_TH_TARGET): $(afw_th_dir)/all_afw_th_files_stamp $(AFW_TH_TEST_CONFIGS)
	$(hide) echo "Package Android for Work Test Harness: $@"
	$(hide) $(ACP) -f $(afw_th_src_dir)/afw-test.props $(PRIVATE_DIR)/testcases
	$(hide) $(ACP) -f $(afw_th_src_dir)/zip_exclude.lst $(afw_th_dir)
	$(hide) $(ACP) -f $(afw_th_src_dir)/apps/SystemUtil/afw-test-system-util-permissions.xml $(PRIVATE_DIR)/testcases
	$(hide) cd $(dir $@) && zip -rq $(notdir $@) $(PRIVATE_NAME) -x@zip_exclude.lst

.PHONY: afw-test-harness
afw-test-harness: $(INTERNAL_AFW_TH_TARGET) adb
	$(hide) echo "********************************************"
	$(hide) echo "To start tradefed, run: afw-test-tradefed"
	$(hide) echo "********************************************"
$(call dist-for-goals,afw-test-harness,$(INTERNAL_AFW_TH_TARGET))

/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.testtype;

import com.android.afwtest.tradefed.TestConfig;
import com.android.ddmlib.Log;
import com.android.tradefed.testtype.AndroidJUnitTest;

import java.util.concurrent.TimeUnit;

/**
 * Test class for Android for Work Test Harness JUnit tests.
 */
public class AfwAndroidJUnitTest extends AndroidJUnitTest {

    private static final String LOG_TAG = "afwtest.AfwAndroidJUnitTest";

    // Default execution timeout for afw test packages
    private static final long TEST_TIMEOUT_MS = TimeUnit.MINUTES.toMillis(10);

    // 1 min in milliseconds
    private static final long ONE_MIN_MS = TimeUnit.MINUTES.toMillis(1);

    /**
     * {@inheritDoc}
     */
    public AfwAndroidJUnitTest() {
        super();

        long testTimeout =
                TimeUnit.MINUTES.toMillis(TestConfig.getInstance().getTestTimeoutMin(0));
        if (testTimeout == 0) {
            testTimeout = TEST_TIMEOUT_MS +
                    TestConfig.getInstance().getTimeoutSize() * ONE_MIN_MS * 5;
        }
        Log.i(LOG_TAG, String.format("Instrumentation test timeout = %d seconds",
                TimeUnit.MILLISECONDS.toSeconds(testTimeout)));
        setTestTimeout((int) testTimeout);
    }
}

/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.afwtest.tradefed.testtype;

import com.android.ddmlib.testrunner.TestIdentifier;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.result.FileInputStreamSource;
import com.android.tradefed.result.ITestInvocationListener;
import com.android.tradefed.result.InputStreamSource;
import com.android.tradefed.result.LogDataType;
import com.android.tradefed.result.ResultForwarder;

import java.io.File;
import java.util.Map;

/**
 * Test invocation listener to capture device screenshot after the test
 */
public class ScreenshotListener extends ResultForwarder {

    private static final String UIAUTOMATOR = "/system/bin/uiautomator";
    private static final String UIAUTOMATOR_DUMP_COMMAND = "dump";
    private static final String UIDUMP_DEVICE_PATH = "/data/local/tmp/uidump.xml";

    final private ITestDevice mDevice;
    private boolean testFailed = false;

    public ScreenshotListener(ITestInvocationListener listener, ITestDevice device) {
        super(listener);
        mDevice = device;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void testEnded(TestIdentifier test, Map<String, String> testMetrics) {
        super.testEnded(test, testMetrics);
        CLog.i("Capturing device screenshot after test %s", test.toString());
        try {
            final InputStreamSource screenSource = mDevice.getScreenshot("PNG",
                    /* rescale */ !testFailed);
            super.testLog(String.format("%s-screenshot", test.toString()), LogDataType.PNG,
                    screenSource);
            screenSource.cancel();
        } catch (DeviceNotAvailableException e) {
            CLog.e(e);
            CLog.e("Device %s became unavailable while capturing screenshot",
                    mDevice.getSerialNumber());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void testFailed(TestIdentifier test, String trace) {
        super.testFailed(test, trace);

        testFailed = true;

        CLog.i("Dumping device ui layout after test %s", test.toString());
        try {
            mDevice.executeShellCommand(
                    String.format("%s %s %s", UIAUTOMATOR, UIAUTOMATOR_DUMP_COMMAND,
                            UIDUMP_DEVICE_PATH));
            final File dumpFile = mDevice.pullFile(UIDUMP_DEVICE_PATH);
            final InputStreamSource dumpSource = new FileInputStreamSource(dumpFile,
                    /* deleteFileOnCancel */ true);
            super.testLog(String.format("%s-uidump", test.toString()), LogDataType.XML, dumpSource);
            dumpSource.cancel();
        } catch (DeviceNotAvailableException e) {
            CLog.e(e);
            CLog.e("Device %s became unavailable while capturing ui layout dump",
                    mDevice.getSerialNumber());
        }
    }
}

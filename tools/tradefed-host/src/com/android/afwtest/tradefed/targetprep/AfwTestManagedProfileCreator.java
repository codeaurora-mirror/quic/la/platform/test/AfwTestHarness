package com.android.afwtest.tradefed.targetprep;

import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.config.Option;
import com.android.tradefed.config.OptionClass;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.targetprep.ITargetCleaner;
import com.android.tradefed.targetprep.ITargetPreparer;
import com.android.tradefed.targetprep.TargetSetupError;

/**
 * A {@link ITargetPreparer} that creates a managed profile in the testing device.
 */
@OptionClass(alias = "afw-test-create-managed-profile")
public class AfwTestManagedProfileCreator extends AfwTestTargetPreparer
        implements ITargetCleaner {

    @Option(name = "remove-after-test",
            description = "Remove work profile after test.")
    private boolean mRemoveAfterTest = true;

    @Option(name = "profile-owner-component",
            description = "Profile owner component to set; optional")
    private String mProfileOwnerComponent = null;

    @Option(name = "profile-owner-apk",
            description = "Profile owner apk path; optional")
    private String mProfileOwnerApk = null;

    /** UserID for user in managed profile.*/
    private int mManagedProfileUserId;

    /**
     * {@inheritDoc}
     */
    @Override
    public void setUp(ITestDevice device, IBuildInfo buildInfo)
            throws TargetSetupError, DeviceNotAvailableException {

        String pmCommand = "pm create-user --profileOf 0 --managed "
                + "TestProfile_" + System.currentTimeMillis();
        String pmCommandOutput = device.executeShellCommand(pmCommand);

        // Extract the id of the new user.
        String[] pmCmdTokens = pmCommandOutput.split("\\s+");
        if (!pmCmdTokens[0].contains("Success:")) {
            throw new TargetSetupError("Managed profile creation failed.");
        }
        mManagedProfileUserId = Integer.parseInt(pmCmdTokens[pmCmdTokens.length-1]);

        // Start managed profile user.
        device.startUser(mManagedProfileUserId);

        CLog.i(String.format("New user created: %d", mManagedProfileUserId));

        if (mProfileOwnerComponent != null && mProfileOwnerApk != null) {
            device.installPackageForUser(
                    getApk(buildInfo, mProfileOwnerApk), true, mManagedProfileUserId);
            String command = String.format("dpm set-profile-owner --user %d %s",
                    mManagedProfileUserId, mProfileOwnerComponent);
            String commandOutput = device.executeShellCommand(command);
            String[] cmdTokens = commandOutput.split("\\s+");
            if (!cmdTokens[0].contains("Success:")) {
                throw new RuntimeException(String.format("Fail to setup %s as profile owner.",
                        mProfileOwnerComponent));
            }

            CLog.i(String.format("%s was set as profile owner of user %d",
                    mProfileOwnerComponent, mManagedProfileUserId));
        }

        // Reboot device to create the apps in managed profile.
        device.reboot();
        device.waitForDeviceAvailable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void tearDown(ITestDevice testDevice, IBuildInfo buildInfo, Throwable throwable)
            throws DeviceNotAvailableException {
        if (mRemoveAfterTest) {
            testDevice.removeUser(mManagedProfileUserId);
        }
    }
}

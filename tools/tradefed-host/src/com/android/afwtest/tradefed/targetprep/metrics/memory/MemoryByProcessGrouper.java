/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.targetprep.metrics.memory;

import static com.android.afwtest.tradefed.targetprep.metrics.MetricsUtils.toMetricValueMapEntry;
import static com.android.afwtest.tradefed.targetprep.metrics.nano.Metrics.InvocationMetrics;
import static com.android.afwtest.tradefed.targetprep.metrics.nano.Metrics.InvocationMetrics.MetricValueMap;
import static com.android.afwtest.tradefed.utils.AdbShellUtils.ProcessInfo;

import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.Map;

/**
 * {@link MemoryInfoGrouper} grouping memory data into a map of process name to total PSS consumed
 * by the process.
 */
public class MemoryByProcessGrouper implements MemoryInfoGrouper {

    private Collection<String> mProcesses;

    public MemoryByProcessGrouper(Collection<String> processes) {
        mProcesses = processes;
    }

    @Override
    public Collection<MetricValueMap.ValuesEntry> group(Collection<ProcessInfo> ps,
            Map<Long, Long> memoryInfo) {
        return ps.stream()
                .filter(processInfo -> mProcesses.contains(processInfo.getName()))
                .map(processInfo ->
                        toMetricValueMapEntry(processInfo.getName(), memoryInfo.getOrDefault(
                                processInfo.getPID(), 0L)))
                .collect(toList());
    }

    @Override
    public int getMetricType() {
        return InvocationMetrics.PSS_BY_PROCESS;
    }
}

/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.targetprep.metrics;

/**
 * Custom checked exception to wrap exceptions from com.android.afwtest.tradefed.targetprep.metrics
 * package.
 */
public final class MetricsCollectorException extends Exception {

    public MetricsCollectorException() {
        super();
    }

    public MetricsCollectorException(String message) {
        super(message);
    }

    public MetricsCollectorException(String message, Throwable cause) {
        super(message, cause);
    }

    public MetricsCollectorException(Throwable cause) {
        super(cause);
    }

    protected MetricsCollectorException(
            String message,
            Throwable cause,
            boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

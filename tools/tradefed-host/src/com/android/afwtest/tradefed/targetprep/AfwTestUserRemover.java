/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.targetprep;

import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.config.Option;
import com.android.tradefed.config.OptionClass;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.targetprep.ITargetCleaner;
import com.android.tradefed.targetprep.TargetSetupError;
import com.android.tradefed.util.RunUtil;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * A target preparer that can remove all non-primary users (user id not 0).
 */
@OptionClass(alias = "afw-test-user-remover")
public class AfwTestUserRemover implements ITargetCleaner {

    @Option(name = "remove-users-before-test",
            description = "Remove all non-primary users before the test.")
    private boolean mRemoveUsersBeforeTest = false;

    @Option(name = "remove-users-after-test",
            description = "Remove all non-primary users after the test.")
    private boolean mRemoveUsersAfterTest = true;

    /**
     * {@inheritDoc}
     */
    @Override
    public void setUp(ITestDevice device, IBuildInfo buildInfo) throws TargetSetupError,
            DeviceNotAvailableException {
        if (mRemoveUsersBeforeTest && !removeUsers(device)) {
            throw new TargetSetupError(
                    "Failed to remove all non-primary users on device " + device.getSerialNumber());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void tearDown(ITestDevice device, IBuildInfo buildInfo, Throwable e)
            throws DeviceNotAvailableException {
        if (mRemoveUsersAfterTest && !removeUsers(device)) {
            throw new RuntimeException(
                    "Failed to remove all non-primary users on device " + device.getSerialNumber());
        }
    }

    /**
     * Remove all non-primary users.
     *
     * @param device the testing device
     * @return {@code true} if all non-primary users are removed, {@code false} otherwise
     */
    private boolean removeUsers(ITestDevice device) throws DeviceNotAvailableException {
        // Ensure package manager is running
        device.waitForDeviceAvailable();

        ArrayList<Integer> users = device.listUsers();

        if (users == null) {
            return true;
        }

        boolean success = true;

        for (Integer userId : users) {
            if (userId == 0) {
                // Can't remove user 0, so don't try.
                continue;
            }

            if (device.removeUser(userId)) {
                CLog.i(String.format("Successfully removed user %d on %s",
                        userId,
                        device.getSerialNumber()));
            } else {
                CLog.e(String.format("Failed to remove user %d on %s",
                        userId,
                        device.getSerialNumber()));
                success = false;
            }
        }

        RunUtil.getDefault().sleep(TimeUnit.SECONDS.toMillis(60));

        // Make sure device is still available
        device.waitForDeviceAvailable();

        return success;
    }
}


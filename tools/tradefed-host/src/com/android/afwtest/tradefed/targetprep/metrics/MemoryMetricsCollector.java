/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.targetprep.metrics;

import static com.android.afwtest.tradefed.targetprep.metrics.nano.Metrics.InvocationMetrics.InvocationMetric;

import com.android.afwtest.tradefed.targetprep.metrics.memory.MemoryByUserGrouper;
import com.android.afwtest.tradefed.targetprep.metrics.memory.MemoryCollectorTask;
import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.device.ITestDevice;

import java.util.Arrays;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Collector of memory usage metrics.
 */
public class MemoryMetricsCollector implements MetricsCollector {

    /**
     * Executor to run scheduled metric collection task.
     */
    private ScheduledExecutorService mExecutor;

    /**
     * {@inheritDoc}
     */
    @Override
    public void init(ITestDevice testDevice, IBuildInfo iBuildInfo,
            Consumer<InvocationMetric> metricsConsumer)
        throws MetricsCollectorException {

        AtomicLong tickCounter = new AtomicLong(0L);

        mExecutor = new ScheduledThreadPoolExecutor(1);
        mExecutor.scheduleWithFixedDelay(getCollectorTask(testDevice, metricsConsumer,
                tickCounter::getAndIncrement), 0, 500, TimeUnit.MILLISECONDS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy() {
        mExecutor.shutdownNow();
    }

    /**
     * Gets {@link Runnable} to process a single iteration of memory information collection.
     *
     * @param testDevice current {@link ITestDevice}.
     * @param metricsConsumer {@link Consumer} to emit collected metrics.
     * @param tickCounter tick counter.
     * @return {@link Runnable} to process a single iteration of memory information collection.
     */
    protected Runnable getCollectorTask(ITestDevice testDevice,
            Consumer<InvocationMetric> metricsConsumer,
            Supplier<Long> tickCounter) {
        return new MemoryCollectorTask(testDevice, metricsConsumer, tickCounter,
                Arrays.asList(new MemoryByUserGrouper()));
    }
}
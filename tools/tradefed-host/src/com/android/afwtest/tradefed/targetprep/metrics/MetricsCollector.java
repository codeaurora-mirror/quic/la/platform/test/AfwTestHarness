/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.targetprep.metrics;

import static com.android.afwtest.tradefed.targetprep.metrics.nano.Metrics.InvocationMetrics.InvocationMetric;

import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.device.ITestDevice;

import java.util.function.Consumer;

/** Collects metrics from a device during test invocation. */
public interface MetricsCollector {

    /**
     * Initializes metrics collector.
     *
     * @param iTestDevice current {@link ITestDevice}.
     * @param iBuildInfo current {@link IBuildInfo}.
     * @param metricsConsumer {@link Consumer} to emit collected metrics.
     * @throws MetricsCollectorException if an error occurred during initialization.
     */
    void init(
            ITestDevice iTestDevice,
            IBuildInfo iBuildInfo,
            Consumer<InvocationMetric> metricsConsumer)
            throws MetricsCollectorException;

    /** Cleans up metrics collector. */
    void destroy();
}

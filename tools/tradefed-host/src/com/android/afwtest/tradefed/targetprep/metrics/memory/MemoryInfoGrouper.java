/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.targetprep.metrics.memory;

import static com.android.afwtest.tradefed.targetprep.metrics.nano.Metrics.InvocationMetrics.MetricValueMap;
import static com.android.afwtest.tradefed.utils.AdbShellUtils.ProcessInfo;

import java.util.Collection;
import java.util.Map;

/**
 * An object to group memory and process information collected from the device into
 * performance metrics.
 */
public interface MemoryInfoGrouper {

    /**
     * Groups memory and process information collected from the device into
     * performance metrics.
     *
     * @param ps process information.
     * @param memoryInfo memory information, map of process id to total PSS consumed be the process.
     * @return {@link Collection} of performance metrics.
     */
    Collection<MetricValueMap.ValuesEntry> group(
            Collection<ProcessInfo> ps,
            Map<Long, Long> memoryInfo);

    /**
     * Gets the type of metrics emitted by the grouper.
     *
     * @return the type of metrics emitted by the grouper.
     */
    int getMetricType();
}

/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.utils;

import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.log.LogUtil.CLog;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Time related utils.
 */
public class TimeUtil {

    /**
     * Return the current timestamp in a compressed format, e.g.: 2015.11.25_11.42.12
     */
    public static String getResultTimestamp() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss");
        return dateFormat.format(new Date());
    }

    /**
     * Sync device time with host.
     *
     * @param device test device
     */
    public static void syncHostTime(ITestDevice device) throws DeviceNotAvailableException {
        CLog.i(String.format("Before time sync: %s", device.executeShellCommand("date -u")));

        Date date = new Date();
        String dateString = null;
        if (device.getApiLevel() < 23) {
            // set date in epoch format
            dateString = Long.toString(date.getTime() / 1000); //ms to s
        } else {
            // set date with POSIX like params
            SimpleDateFormat sdf = new java.text.SimpleDateFormat("MMddHHmmyyyy.ss");
            sdf.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
            dateString = sdf.format(date);
        }
        // best effort, no verification
        String cmd = String.format("date -u %s", dateString);
        CLog.i(String.format("Setting time: %s", cmd));
        CLog.i(device.executeShellCommand(cmd));

        CLog.i(String.format("After time sync: %s", device.executeShellCommand("date -u")));
    }

}

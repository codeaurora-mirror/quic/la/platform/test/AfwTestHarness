/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.utils;

import com.android.tradefed.log.LogUtil.CLog;

import java.io.IOException;
import java.io.InputStream;

/**
 * {@link Runnable} that reads and discards data from specified stream.
 */
public final class StreamDiscarder implements Runnable {

    private static final int BATCH_SIZE = 1024;

    private final InputStream mStream;
    private final byte[] buffer;

    /**
     * Constructs {@link StreamDiscarder} instance.
     *
     * @param is {@link InputStream} to read and discard data from.
     */
    public StreamDiscarder(InputStream is) {
        mStream = is;
        buffer = new byte[BATCH_SIZE];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("StatementWithEmptyBody")
    public void run() {
        try {
            while (!Thread.interrupted() && mStream.read(buffer) > 0) {}
        } catch (IOException e) {
            CLog.e(e);
        }
    }
}

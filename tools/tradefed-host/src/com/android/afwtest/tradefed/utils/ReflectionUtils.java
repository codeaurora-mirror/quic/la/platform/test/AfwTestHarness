/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/** Set of utility methods for reflection operations. */
public final class ReflectionUtils {

    /** Prevent class from instantiation. */
    private ReflectionUtils() {}

    /**
     * Creates instance of specified class.
     *
     * @param className name of the class to instantiate.
     * @param clazz {@link Class} extended by specified class to cast result to.
     * @param args arguments of constructor to invoke.
     * @return instance of specified class.
     * @throws ClassNotFoundException if specified class cannot be found in classpath.
     * @throws NoSuchMethodException if no constructor can be found suitable for specified
     *     constructor arguments.
     * @throws IllegalAccessException if access to suitable constructor is forbidden.
     * @throws InstantiationException if the class that declares the underlying constructor
     *     represents an abstract class.
     * @throws InvocationTargetException if the underlying constructor throws an exception.
     * @throws IllegalArgumentException if class specified by name is not assignable to the class
     *     specified by class instance.
     */
    @SuppressWarnings("unchecked")
    public static <T> T getInstanceOf(String className, Class<T> clazz, Object... args)
            throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException,
                    InstantiationException, InvocationTargetException {

        Class<?> resolvedClass = clazz.getClassLoader().loadClass(className);
        if (!clazz.isAssignableFrom(resolvedClass)) {
            throw new IllegalArgumentException(
                    String.format(
                            "An instance of %s cannot be assigned to %s.",
                            resolvedClass.getName(), clazz.getName()));
        }
        Constructor<T> constructor =
                (Constructor<T>) resolvedClass.getConstructor(getClassesOf(args));
        return constructor.newInstance(args);
    }

    /**
     * Gets classes of specified objects.
     *
     * @param objects objects to get classes of.
     * @return classes of specified objects.
     */
    private static Class<?>[] getClassesOf(Object... objects) {
        Class<?>[] result = new Class[objects.length];
        for (int i = 0; i < objects.length; i++) {
            result[i] = objects[i].getClass();
        }
        return result;
    }
}
